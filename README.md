# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	SapceX Launch Program : Demo application submitted as an assingment.
* Version
	v0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	Angular 9 is used to create a single page application. Application is hosted on Firebase.
* Configuration
	Angular v9
* Dependencies
	Bootstrap
	ngx-spinner
	Http Client
* Database configuration
	DB not required
* How to run tests
	URL : https://spacex-program-launch.web.app/
* Deployment instructions
	Firebase is used
* Design
	Application is responsive
	Click Filter btn to apply filters. Seperate service is used to fetch data.
	3 Major Components
		1. Header Component
			Display Header of the application
			Filter BTN (icon)
		2. Filter Component
			List of Filters (Year, Landing, Launching)
		3. Space Launches Component
			Dispaly Filtered data in Tiles
	Services
		1. Back-end Service
			To call api(s)
		2. Utills Service
			Common service to set global data
		3. Filter Service
			Common service to fetch and set filtered data.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
	SARTHAK KULSHRESHTHA
	KULSHRESHTHASARTHAK@GMAIL.COM
	+91-9971339984