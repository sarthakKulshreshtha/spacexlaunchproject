import { Component, OnInit } from '@angular/core';
import { UtilsService } from './../../services/utils.service';
import { BackEndService } from './../../services/back-end.service';



@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss', './../../app/app.component.scss']
})
export class FilterComponent implements OnInit {

  years = [];
  flag = [];

  constructor(public utills: UtilsService, private api: BackEndService) { }

  ngOnInit(): void {
    this.setSpaceFilters();
  }


  setSpaceFilters() {
    // let filterHeadings = ["Launch Years", "Successful Launch", "Successful Landing"];
    if ((this.utills.spaceFilters).length == 0) {
      this.utills.spaceFilters = [{
        type: 'year',
        heading: "Launch Years",
        filters: this.fetchLaunchYears()
      },
      {
        type: 'launch',
        heading: "Successful Launch",
        filters: this.setFilterFlags()
      },
      {
        type: 'landing',
        heading: "Successful Landing",
        filters: this.setFilterFlags()
      }]
    }
  }

  fetchLaunchYears() {
    let currentYear = new Date().getFullYear();
    for (let i = currentYear; i > (currentYear - this.utills.yearsRange); i--) {
      let temp = {
        flag: i,
        selected: false
      }
      this.years.push(temp)
    }
    return this.years;
  }

  setFilterFlags() {
    this.flag = [{
      flag: 'true',
      selected: false
    }, {
      flag: 'false',
      selected: false
    }];
    return this.flag;
  }

  setFilter(type, flag) {
    this.utills.spaceFilters.forEach((element) => {
      if (element.type == type) {
        (element.filters).forEach((filterData) => {
          if (filterData.flag == flag) {
            filterData.selected = !(filterData.selected);
          } else {
            filterData.selected = false
          }
        });
      }
    })
  }



}
