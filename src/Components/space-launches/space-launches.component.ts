import { Component, OnInit } from '@angular/core';
import { UtilsService } from './../../services/utils.service';
import { FetchSpaceLaunchDataService } from './../../services/fetch-space-launch-data.service';
import { NgxSpinnerService } from "ngx-spinner";  

@Component({
  selector: 'app-space-launches',
  templateUrl: './space-launches.component.html',
  styleUrls: ['./space-launches.component.scss', './../../app/app.component.scss']
})
export class SpaceLaunchesComponent implements OnInit {

  errorImg = './assets/demo.jpg';


  constructor(public utills: UtilsService, private fetchSpaceLaunchData: FetchSpaceLaunchDataService, public spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.utills.spaceLaunches = [];
    this.fetchSpaceLaunchData.fetchSapceLaunch();
  }

  setFilteredData(event) {
    console.log('called');
  }



}
