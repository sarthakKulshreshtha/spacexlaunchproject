import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FetchSpaceLaunchDataService } from './../../services/fetch-space-launch-data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', './../../app/app.component.scss']
})




export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal, private fetchSpaceLaunchData: FetchSpaceLaunchDataService) { }

  ngOnInit(): void {
  }

  showFilterModal(id) {
    this.modalService.open(id, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  applyFilters(event) {
    this.fetchSpaceLaunchData.fetchFilteredData();
  }
}
