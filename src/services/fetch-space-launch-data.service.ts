import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service'
import { BackEndService } from './back-end.service'
import { NgxSpinnerService } from "ngx-spinner";




@Injectable({
  providedIn: 'root'
})
export class FetchSpaceLaunchDataService {

  constructor(public utills: UtilsService, public api: BackEndService, public spiner: NgxSpinnerService) { }

  fetchSapceLaunch() {
    this.spiner.show();
    this.utills.spaceLaunches = [];
    this.api.fetchSapceLaunchApi().subscribe((data: []) => {
      this.setSpaceLaunchesObject(data);
      this.spiner.hide();
    })
  }

  fetchFilteredData() {
    this.spiner.show();
    this.utills.spaceLaunches = [];
    let filterObject = {
      'year': 'launch_year',
      'landing': 'land_success',
      'launch': 'launch_success'
    }
    let filters = "";
    (this.utills.spaceFilters).forEach((element) => {
      (element.filters).forEach(filter => {
        if (filter.selected) {
          filters += `&${filterObject[element.type]}=${filter.flag}`
        }
      });
    })
    if (!filters) {
      this.spiner.hide();
      this.fetchSapceLaunch();
    } else {
      this.api.fetchFilteredSapceLaunchApi(filters).subscribe((data: []) => {
        this.setSpaceLaunchesObject(data);
        this.spiner.hide();
      })
    }
  }

  setSpaceLaunchesObject(data) {
    data.forEach((element: object) => {
      let temp = {
        mission_name: element['mission_name'],
        flight_number: element['flight_number'],
        launch_year: element['launch_year'],
        launch_success: element['launch_success'],
        land_success: element['rocket'].first_stage.cores[0].land_success,
        mission_id: element['mission_id'],
        img: element['links'].mission_patch
      }
      this.utills.spaceLaunches.push(temp);
    });
  }


}
