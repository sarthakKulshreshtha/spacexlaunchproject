import { TestBed } from '@angular/core/testing';

import { FetchSpaceLaunchDataService } from './fetch-space-launch-data.service';

describe('FetchSpaceLaunchDataService', () => {
  let service: FetchSpaceLaunchDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FetchSpaceLaunchDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
