import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class BackEndService {

  constructor(private http: HttpClient, private utils: UtilsService) { }

  fetchSapceLaunchApi() {
    return this.http.get(this.utils.baseUrl);
  }

  fetchFilteredSapceLaunchApi(filter) {
    return this.http.get(this.utils.baseUrl + filter);
  }


}
